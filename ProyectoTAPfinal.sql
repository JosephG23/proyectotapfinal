SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `creditos` int(11) NOT NULL,
  `juegos` int(11) NOT NULL,
  `ganados` int(11) NOT NULL,
  `perdidos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cuenta` (`idCuenta`, `creditos`, `juegos`, `ganados`, `perdidos`) VALUES
(1, 0, 0, 0, 0),
(2, 0, 0, 0, 0),
(3, 2005, 6, 5, 1);

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `idCuenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuarios` (`idUsuario`, `nombreUsuario`, `password`, `idCuenta`) VALUES
(1, 'Raymundo', 'ray123', 2),
(2, 'Lupita', '1234', 3);

ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;