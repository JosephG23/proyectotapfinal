package Cliente;
import InicioSesion.Conexion;
import java.awt.Color;
import java.util.Random;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class panelJuego extends javax.swing.JPanel {
    private final ImageIcon iconos[] = new ImageIcon[3];
    private final String images[] = {"marcoArbolito.png", "marcoPalmita.png", "marcoPinito.png"};
    private final Color colores[] = {new Color(84,253,232), new Color(215,132,231), new Color(243,133,106)};
    private final int indices[] = new int[3];
    private final int giros[] = new int[3];
    Random random = new Random();
    Vector<JLabel> labels = new Vector<>();
    private final String usuario;

    public panelJuego(String usuario, String creditos) {
        initComponents();
        this.usuario = usuario;
        this.valorCreditos.setText(creditos);

        for(int i=0; i<3; i++){
            indices[i] = -1;
            iconos[i] = (new ImageIcon(this.getClass().getResource("/resources/" + images[i])));
        }
        labels.add(etiquetaArbol);
        labels.add(etiquetaPalma);
        labels.add(etiquetaPino);
    }

    public void jugar(){
        for(int i=0; i<3; i++)
            giros[i] = random.nextInt((50 - 20) + 1) + 20;

        Timer timer = new Timer(100, null);
        timer.addActionListener((e) -> {

            boolean band = true;

            for(int i=0; i<3; i++){
                if(giros[i] == 0)
                    continue;
                band = false;
                giros[i]--;
                indices[i] = (indices[i] + 1) % 3;
                labels.get(i).setIcon(iconos[indices[i]]);
                labels.get(i).setBackground(colores[indices[i]]);
            }

            if(band){
                checarResultado();
                timer.stop();
            }
            else timer.restart();
        });

        timer.start();
    }

    public void checarResultado(){

        int apostar  = Integer.parseInt(valorApuesta.getText());
        int creditos = Integer.parseInt(valorCreditos.getText());
        Conexion cliente = new Conexion();

        if(indices[0] == indices[1] && indices[1] == indices[2]){
            apostar *= 2;
            creditos += apostar;
            etiquetaResultado.setText("¡ HAS GANADO !");
            cliente.enviarMensaje("NuevoJuego " + usuario + " " + creditos + " Ganaste ");
        }
        else if(indices[0] == indices[1] || indices[0] == indices[2] || indices[1] == indices[2]){
            apostar /= 2;
            creditos += apostar;
            etiquetaResultado.setText("¡ B I E N !");
            cliente.enviarMensaje("NuevoJuego " + usuario + " " + creditos + " Ganaste ");
        }
        else {
            etiquetaResultado.setText(" P E R D I S T E  :'(  ");
            creditos -= apostar;
            cliente.enviarMensaje("NuevoJuego " + usuario + " " + creditos + " Perdiste ");
        }
        valorCreditos.setText(Long.toString(creditos));

    }


    @SuppressWarnings("unchecked")
    private void initComponents() {

        panelEtiquetas = new javax.swing.JPanel();
        panelCreditos = new javax.swing.JPanel();
        etiquetaCreditos = new javax.swing.JLabel();
        valorCreditos = new javax.swing.JLabel();
        panelApuesta = new javax.swing.JPanel();
        etiquetaApuesta = new javax.swing.JLabel();
        valorApuesta = new javax.swing.JLabel();
        panelImagenes = new javax.swing.JPanel();
        etiquetaArbol = new javax.swing.JLabel();
        etiquetaPalma = new javax.swing.JLabel();
        etiquetaPino = new javax.swing.JLabel();
        panelResultado = new javax.swing.JPanel();
        etiquetaResultado = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        botonInsertar = new javax.swing.JButton();
        botonRetirar = new javax.swing.JButton();
        botonApostar = new javax.swing.JButton();
        botonJugar = new javax.swing.JButton();

        setBackground(new java.awt.Color(235, 240, 169));
        setPreferredSize(new java.awt.Dimension(550, 500));

        panelEtiquetas.setBackground(getBackground());
        panelEtiquetas.setPreferredSize(new java.awt.Dimension(550, 100));
        panelEtiquetas.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 0, 5));

        panelCreditos.setBackground(getBackground());
        panelCreditos.setPreferredSize(new java.awt.Dimension(180, 80));

        etiquetaCreditos.setFont(new java.awt.Font("SansSerif", 1, 18));
        etiquetaCreditos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaCreditos.setText("TUS CRÉDITOS");
        etiquetaCreditos.setPreferredSize(new java.awt.Dimension(150, 30));
        panelCreditos.add(etiquetaCreditos);

        valorCreditos.setFont(new java.awt.Font("SansSerif", 1, 20));
        valorCreditos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        valorCreditos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marcoc.png")));
        valorCreditos.setText("1000");
        valorCreditos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panelCreditos.add(valorCreditos);

        panelEtiquetas.add(panelCreditos);

        panelApuesta.setBackground(getBackground());
        panelApuesta.setPreferredSize(new java.awt.Dimension(180, 80));

        etiquetaApuesta.setFont(new java.awt.Font("SansSerif", 1, 18));
        etiquetaApuesta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaApuesta.setText("TU APUESTA");
        etiquetaApuesta.setPreferredSize(new java.awt.Dimension(150, 30));
        panelApuesta.add(etiquetaApuesta);

        valorApuesta.setFont(new java.awt.Font("SansSerif", 1, 20));
        valorApuesta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        valorApuesta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marcoc.png")));
        valorApuesta.setText("1000");
        valorApuesta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panelApuesta.add(valorApuesta);

        panelEtiquetas.add(panelApuesta);

        add(panelEtiquetas);

        panelImagenes.setBackground(getBackground());
        panelImagenes.setPreferredSize(new java.awt.Dimension(550, 220));

        etiquetaArbol.setBackground(new java.awt.Color(84, 253, 232));
        etiquetaArbol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaArbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marcoArbolito.png")));
        etiquetaArbol.setLabelFor(etiquetaPalma);
        etiquetaArbol.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaArbol.setOpaque(true);
        panelImagenes.add(etiquetaArbol);

        etiquetaPalma.setBackground(new java.awt.Color(215, 132, 231));
        etiquetaPalma.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaPalma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marcoPalmita.png")));
        etiquetaPalma.setLabelFor(etiquetaPalma);
        etiquetaPalma.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaPalma.setOpaque(true);
        panelImagenes.add(etiquetaPalma);

        etiquetaPino.setBackground(new java.awt.Color(243, 133, 106));
        etiquetaPino.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaPino.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marcoPinito.png")));
        etiquetaPino.setLabelFor(etiquetaPalma);
        etiquetaPino.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaPino.setOpaque(true);
        panelImagenes.add(etiquetaPino);

        add(panelImagenes);

        panelResultado.setBackground(getBackground());
        panelResultado.setPreferredSize(new java.awt.Dimension(550, 90));

        etiquetaResultado.setBackground(getBackground());
        etiquetaResultado.setFont(new java.awt.Font("SansSerif", 1, 22));
        etiquetaResultado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaResultado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/marco7b.png")));
        etiquetaResultado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaResultado.setOpaque(true);
        panelResultado.add(etiquetaResultado);

        add(panelResultado);

        panelBotones.setBackground(getBackground());
        panelBotones.setPreferredSize(new java.awt.Dimension(550, 100));

        botonInsertar.setFont(new java.awt.Font("SansSerif", 1, 13));
        botonInsertar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/boton4.png")));
        botonInsertar.setText("INSERTAR");
        botonInsertar.setBorderPainted(false);
        botonInsertar.setContentAreaFilled(false);
        botonInsertar.setFocusPainted(false);
        botonInsertar.setFocusable(false);
        botonInsertar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonInsertar.setName("botonInsertar");
        botonInsertar.setPreferredSize(new java.awt.Dimension(100, 90));
        botonInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonInsertarActionPerformed(evt);
            }
        });
        panelBotones.add(botonInsertar);

        botonRetirar.setFont(new java.awt.Font("SansSerif", 1, 13));
        botonRetirar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/boton4.png")));
        botonRetirar.setText("RETIRO");
        botonRetirar.setBorderPainted(false);
        botonRetirar.setContentAreaFilled(false);
        botonRetirar.setFocusPainted(false);
        botonRetirar.setFocusable(false);
        botonRetirar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonRetirar.setName("botonInsertar");
        botonRetirar.setPreferredSize(new java.awt.Dimension(100, 90));
        botonRetirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRetirarActionPerformed(evt);
            }
        });
        panelBotones.add(botonRetirar);

        botonApostar.setFont(new java.awt.Font("SansSerif", 1, 13));
        botonApostar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/boton4.png")));
        botonApostar.setText("APOSTAR");
        botonApostar.setBorderPainted(false);
        botonApostar.setContentAreaFilled(false);
        botonApostar.setFocusPainted(false);
        botonApostar.setFocusable(false);
        botonApostar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonApostar.setName("botonInsertar");
        botonApostar.setPreferredSize(new java.awt.Dimension(100, 90));
        botonApostar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonApostarActionPerformed(evt);
            }
        });
        panelBotones.add(botonApostar);

        botonJugar.setFont(new java.awt.Font("SansSerif", 1, 13));
        botonJugar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/boton4.png")));
        botonJugar.setText("JUGAR");
        botonJugar.setBorderPainted(false);
        botonJugar.setContentAreaFilled(false);
        botonJugar.setFocusPainted(false);
        botonJugar.setFocusable(false);
        botonJugar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonJugar.setName("botonInsertar");
        botonJugar.setPreferredSize(new java.awt.Dimension(100, 90));
        botonJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonJugarActionPerformed(evt);
            }
        });
        panelBotones.add(botonJugar);

        add(panelBotones);
    }

    private void botonInsertarActionPerformed(java.awt.event.ActionEvent evt) {
        String creditos = JOptionPane.showInputDialog("¿Cuantos creditos quieres agregar?");
        try{
            String currentCredit = valorCreditos.getText();
            int cCredit = Integer.parseInt(currentCredit);
            int credit = Integer.parseInt(creditos);

            valorCreditos.setText(Integer.toString(cCredit + credit));

            Conexion cliente = new Conexion();

            cliente.enviarMensaje("actualizarCreditos "+ usuario + " " + Integer.toString(cCredit + credit));
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(this, "Debes de agreagar numeros positivos");
        }
    }

    private void botonRetirarActionPerformed(java.awt.event.ActionEvent evt) {
        String retirar = JOptionPane.showInputDialog("¿Cuánto quiere retirar");
        try{
            int check = Integer.parseInt(retirar);
            if(check < 1){
                JOptionPane.showMessageDialog(this, "Debes de agregar números positivos");
                return;
            }

            int credit = Integer.parseInt(valorCreditos.getText());
            if(check > credit){
                JOptionPane.showMessageDialog(this, "No cuenta con suficientes créditos");
                return;
            }

            credit -= check;
            valorCreditos.setText(credit + "");

            Conexion cliente = new Conexion();
            cliente.enviarMensaje("actualizaCreditos " + usuario + " " + Integer.toString(credit));

        } catch(NumberFormatException ex){
            JOptionPane.showConfirmDialog(this, "Debes de agregar números positivos");
        }
    }

    private void botonApostarActionPerformed(java.awt.event.ActionEvent evt) {
        String apuesta = JOptionPane.showInputDialog("¿Cuanto desea apostar?");

        try{
            int check = Integer.parseInt(apuesta);
            if(check < 1){
                JOptionPane.showMessageDialog(this, "Debes de agreagar numeros positivos");
                return;


            }
            valorApuesta.setText(apuesta);
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(this, "Debes de agreagar numeros positivos");
        }
    }

    private void botonJugarActionPerformed(java.awt.event.ActionEvent evt) {
        etiquetaResultado.setText("");
        jugar();
    }
    private javax.swing.JButton botonApostar;
    private javax.swing.JButton botonInsertar;
    private javax.swing.JButton botonJugar;
    private javax.swing.JButton botonRetirar;
    private javax.swing.JLabel etiquetaApuesta;
    private javax.swing.JLabel etiquetaArbol;
    private javax.swing.JLabel etiquetaCreditos;
    private javax.swing.JLabel etiquetaPalma;
    private javax.swing.JLabel etiquetaPino;
    private javax.swing.JLabel etiquetaResultado;
    private javax.swing.JPanel panelApuesta;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelCreditos;
    private javax.swing.JPanel panelEtiquetas;
    private javax.swing.JPanel panelImagenes;
    private javax.swing.JPanel panelResultado;
    private javax.swing.JLabel valorApuesta;
    private javax.swing.JLabel valorCreditos;

    public static void main(String args[]){
        JFrame f = new JFrame("Juego final TAP");
        panelJuego game = new panelJuego("", "0");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(game);
        f.setSize(520, 630);
        f.setLocation(100, 100);
        f.setVisible(true);
    }

}