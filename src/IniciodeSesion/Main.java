package InicioSesion;

import javax.swing.*;

public class Main {
    
    public static void main(String args[]){
        JFrame frame = new JFrame();
        Login login = new Login();
        OyenteLogin oyente = new OyenteLogin(login, frame);
        login.addEventos(oyente);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);
        frame.setSize(410, 257);
        frame.add(login);
        frame.setVisible(true);
    }    
    
}
