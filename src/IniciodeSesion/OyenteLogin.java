package InicioSesion;

import Cliente.panelJuego;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class OyenteLogin implements ActionListener{
    private Conexion conexion;
    private final Login login;
    private final JFrame frame;
    
    public OyenteLogin(Login login, JFrame f){
        this.login = login;
        this.frame = f;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boton = (JButton) e.getSource();
        switch(boton.getName()){
            case "iniciar":
                try{
                    conexion = new Conexion();
                    String usuario = login.getTextUsuario().getText();
                    String password = new String(login.getTextPassword().getPassword());
                    conexion.enviarMensaje("Login " + usuario + " " + password);
                    String mensaje = conexion.recibirMensaje();
                    procesaMensaje(usuario, mensaje);
                }catch(IOException ex){
                    System.out.println(ex.getMessage());
                } catch (InterruptedException ex) {}
                break;
            
            case "nuevo":
                System.out.println("Creando cuenta..");
                new crearCuenta().setVisible(true);
                break;
            
            case "cancelar":
                System.exit(0);
                break;
        }
    }
    
    private void procesaMensaje(String usuario, String mensaje) throws InterruptedException, IOException {
      
        System.out.println("Mensaje: " + mensaje);
        switch(mensaje){
            case "OK":
                conexion = new Conexion();
                conexion.enviarMensaje("Tabla " + usuario);
                frame.setVisible(false);
                String creditos = conexion.recibirMensaje();
                System.out.println(creditos);
                panelJuego game = new panelJuego(usuario, creditos);
                JFrame frame = new JFrame("Juego");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(450, 555);
                frame.setLocation(100, 100);
                frame.add(game);
                frame.setVisible(true);
                break;
            
            case "NotFound":
                JOptionPane.showMessageDialog(null, "No existe ese nombre de usuario");
                break;
            
            case "BadPassword":
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta");
                break;
        }
    }
}
