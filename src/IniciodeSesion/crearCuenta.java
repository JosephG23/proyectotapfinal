package InicioSesion;
import java.awt.Color;
import java.io.IOException;
import javax.swing.JOptionPane;

public class crearCuenta extends javax.swing.JFrame {

    public crearCuenta() {
        getContentPane().setBackground(new Color(235,240,169));
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        panelCrearCuenta = new javax.swing.JPanel();
        panelTitulo = new javax.swing.JPanel();
        tituloLogin = new javax.swing.JLabel();
        panelDatos = new javax.swing.JPanel();
        labelUsuario = new javax.swing.JLabel();
        textUsuario = new javax.swing.JTextField();
        labelContrasena = new javax.swing.JLabel();
        textPassword = new javax.swing.JPasswordField();
        botonNuevaCuenta = new javax.swing.JButton();
        botonCancelar = new javax.swing.JButton();
        panelBotones = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 102, 102));
        setPreferredSize(new java.awt.Dimension(370, 200));
        getContentPane().setLayout(new java.awt.FlowLayout());

        panelCrearCuenta.setBackground(new java.awt.Color(255, 153, 153));
        panelCrearCuenta.setAlignmentX(0.0F);
        panelCrearCuenta.setAlignmentY(0.0F);
        panelCrearCuenta.setAutoscrolls(true);
        panelCrearCuenta.setMaximumSize(new java.awt.Dimension(400, 270));
        panelCrearCuenta.setMinimumSize(new java.awt.Dimension(0, 0));
        panelCrearCuenta.setPreferredSize(new java.awt.Dimension(370, 200));
        panelCrearCuenta.setLayout(new java.awt.BorderLayout());

        panelTitulo.setBackground(getBackground());

        tituloLogin.setFont(new java.awt.Font("SansSerif", 1, 18));
        tituloLogin.setText("Nueva cuenta");
        panelTitulo.add(tituloLogin);

        panelCrearCuenta.add(panelTitulo, java.awt.BorderLayout.NORTH);

        panelDatos.setBackground(getBackground());
        panelDatos.setPreferredSize(new java.awt.Dimension(200, 80));
        panelDatos.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 1));

        labelUsuario.setFont(new java.awt.Font("SansSerif", 1, 15));
        labelUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelUsuario.setText("Usuario:");
        labelUsuario.setPreferredSize(new java.awt.Dimension(100, 30));
        panelDatos.add(labelUsuario);

        textUsuario.setColumns(13);
        textUsuario.setFont(new java.awt.Font("SansSerif", 0, 15));
        panelDatos.add(textUsuario);

        labelContrasena.setFont(new java.awt.Font("SansSerif", 1, 15));
        labelContrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelContrasena.setText("Contraseña:");
        labelContrasena.setPreferredSize(new java.awt.Dimension(100, 30));
        panelDatos.add(labelContrasena);

        textPassword.setColumns(13);
        textPassword.setFont(new java.awt.Font("SansSerif", 0, 15));
        panelDatos.add(textPassword);

        botonNuevaCuenta.setFont(new java.awt.Font("SansSerif", 0, 13));
        botonNuevaCuenta.setText("Crear cuenta");
        botonNuevaCuenta.setName("nuevo");
        botonNuevaCuenta.setPreferredSize(new java.awt.Dimension(120, 30));
        botonNuevaCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevaCuentaActionPerformed(evt);
            }
        });
        panelDatos.add(botonNuevaCuenta);

        botonCancelar.setFont(new java.awt.Font("SansSerif", 0, 13));
        botonCancelar.setText("Cancelar");
        botonCancelar.setName("cancelar");
        botonCancelar.setPreferredSize(new java.awt.Dimension(100, 30));
        botonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCancelarActionPerformed(evt);
            }
        });
        panelDatos.add(botonCancelar);

        panelCrearCuenta.add(panelDatos, java.awt.BorderLayout.CENTER);

        panelBotones.setBackground(getBackground());
        panelBotones.setPreferredSize(new java.awt.Dimension(380, 50));
        panelCrearCuenta.add(panelBotones, java.awt.BorderLayout.SOUTH);

        getContentPane().add(panelCrearCuenta);

        pack();
    }

    private void botonNuevaCuentaActionPerformed(java.awt.event.ActionEvent evt) {

        Conexion conn = new Conexion();
        String usuario = textUsuario.getText();
        String password = new String(textPassword.getPassword());
        conn.enviarMensaje("Crear nueva cuenta " + usuario + " " + password);
        System.out.println("Envié el mensaje");
        try {
            String mensaje = conn.recibirMensaje();
            System.out.println("Mensaje: " + mensaje);
            if(mensaje.equals("OK")){
                int res = JOptionPane.showOptionDialog(null, "Cuenta de usuario creada exitosamente", "",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                this.dispose();
            } else{
                int res = JOptionPane.showOptionDialog(null, "El nombre de usuario ya existe elije otro", "",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            }
        } catch (IOException ex) {}
    }

    private void botonCancelarActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }
    private javax.swing.JButton botonCancelar;
    private javax.swing.JButton botonNuevaCuenta;
    private javax.swing.JLabel labelContrasena;
    private javax.swing.JLabel labelUsuario;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelCrearCuenta;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JPasswordField textPassword;
    private javax.swing.JTextField textUsuario;
    private javax.swing.JLabel tituloLogin;
}