package Servidor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import javax.swing.table.DefaultTableModel;

public class OyenteServidor implements ActionListener{
    private Connection conect;
    private PanelServidor main;
    private final int PORT = 1234;
    
    public OyenteServidor(PanelServidor panel)
    {
        try {
            this.main = panel;
            Class.forName("com.mysql.cj.jdbc.Driver");
            conect = DriverManager.getConnection("jdbc:mysql://localhost:3307/casinogame?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            System.out.println("Conexion exitosa");
            
            new Hilo().start();
            } catch (Exception ex) 
                {
                    System.out.println(ex.getMessage());
                    System.out.println("No se pudo conectar con la base de datos");
                }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton closed = (JButton) e.getSource();
        switch(closed.getName()){
            case "cerrar":
                System.exit(0);
                
        }
    }
    
    
    public class Hilo extends Thread{
        private DataInputStream entry;
        private DataOutputStream exit;
        private String line [];
        
        @Override
        public void run(){
            
            try {
                ServerSocket servidor = new ServerSocket(PORT);
                Socket socket = null;
                
                for(;;){
                    socket = servidor.accept();
                    entry = new DataInputStream(socket.getInputStream());
                    exit = new DataOutputStream(socket.getOutputStream());

                    String mensaje = entry.readUTF();
                    System.out.println("msm: " + mensaje);
                    line = mensaje.split(" ");
                    switch (line[0]){
                        case "Login":
                            inicioSesion();
                            break;
                        case "Tabla":
                            actualizaTabla();
                            break;
                        case "actualizaCreditos":
                            actualizaCreditos();
                            break;
                        case "NuevoJuego":
                            actualizaJuegos();
                            break;
                        case "NuevaCuenta":
                            crearCuenta();
                            break;
                        
                    }
                    socket.close();
                }
            } catch (IOException ex) {}
              catch (SQLException ex) {}

        }
        
        private void inicioSesion() throws IOException {
            
            try {
                String consulta = "SELECT count(idUsuario) FROM usuarios WHERE nombreUsuario=\"" + line[1] + "\"";
                System.out.println("Consulta: " + consulta);
                Statement query = conect.createStatement();
                ResultSet result = query.executeQuery(consulta);
                
                int numFila = 0;
                if (result.next()) {
                    numFila = result.getInt(1);
                }
                
                System.out.println("I stay here"); //ignr
                
                if (numFila == 1) {
                    consulta = "SELECT password FROM usuarios WHERE nombreUsuario=\"" + line[1] + "\"";
                    result = query.executeQuery(consulta);
                    String password = "";
                    
                    if (result.next()) {
                        password = result.getString(1);
                    }
                    
                    if (line[2].equals(password)) {
                        exit.writeUTF("OK");
                    } 
                    else {
                        exit.writeUTF("BadPassword");
                    }
                } 
                else {
                    exit.writeUTF("NotFound");
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void actualizaTabla() throws IOException, SQLException {
            String query;
            Statement stmt;
            ResultSet result;
            
            DefaultTableModel model = (DefaultTableModel) main.getTablaUsuarios().getModel();
            Object[] row = new Object[5];
            query = "SELECT nombreUsuario, creditos, juegos, ganados, perdidos FROM cuenta, usuarios WHERE nombreUsuario = \""
                    + line[1] + "\" AND usuarios.idCuenta = cuenta.idCuenta";
            
            stmt = conect.createStatement();
            result = stmt.executeQuery(query);
            
            if (result.next()) {
                for (int i = 0; i < 5; i++) {
                    row[i] = result.getObject(i + 1);
                }
            }
            exit.writeUTF(Integer.toString((Integer) row[1]));
            model.addRow(row);
            main.getTablaUsuarios().setModel(model);

                
        }
        
        private void actualizaCreditos() {
            String query = "UPDATE cuenta SET creditos = " + line[2] + " WHERE idCuenta = "
                    + "(SELECT idCuenta FROM usuarios WHERE nombreUsuario = \""
                    + line[1] + "\");";
            Statement stmt;
            try {
                stmt = conect.createStatement();
                int r = stmt.executeUpdate(query);
                DefaultTableModel modelo = (DefaultTableModel) main.getTablaUsuarios().getModel();
                int rows = modelo.getRowCount();
                for (int i = 0; i < rows; i++) {
                    String nombre = (String) modelo.getValueAt(i, 0);
                    if (nombre.equals(line[1])) {
                        modelo.setValueAt(line[2], i, 1);
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void actualizaJuegos() {
            String query;
            if (line[3].equals("Win")) {
                query = "UPDATE cuenta SET creditos = " + line[2] + ", ganados = ganados + 1, juegos = juegos + 1  WHERE idCuenta = "
                        + "(SELECT idCuenta FROM usuarios WHERE nombreUsuario = \""
                        + line[1] + "\");";
            } else {
                query = "UPDATE cuenta SET creditos = " + line[2] + ", perdidos = perdidos + 1, juegos = juegos + 1 WHERE idCuenta = "
                        + "(SELECT idCuenta FROM usuarios WHERE nombreUsuario = \""
                        + line[1] + "\");";
            }
            try {
                Statement stmt = conect.createStatement();
                int r = stmt.executeUpdate(query);
                DefaultTableModel modelo = (DefaultTableModel) main.getTablaUsuarios().getModel();
                int rows = modelo.getRowCount();
                for (int i = 0; i < rows; i++) {
                    String nombre = (String) modelo.getValueAt(i, 0);
                    int win = (int) modelo.getValueAt(i, 3);
                    int loss = (int) modelo.getValueAt(i, 4);
                    int juegos = (int) modelo.getValueAt(i, 2);
                    if (nombre.equals(line[1])) {
                        modelo.setValueAt(line[2], i, 1);
                        juegos++;
                        modelo.setValueAt(juegos, i, 2);
                        if (line[3].equals("Winner")) {
                            win++;
                            modelo.setValueAt(win, i, 3);
                        } else {
                            loss++;
                            modelo.setValueAt(loss, i, 4);
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void crearCuenta() throws IOException{
            try {
                String consulta = "SELECT count(idUsuario) FROM usuarios WHERE nombreUsuario=\"" + line[1] + "\"";
                Statement query = conect.createStatement();
                ResultSet resultado = query.executeQuery(consulta);
                int numFila = 0;
                if (resultado.next()) 
                    numFila = resultado.getInt(1);
                
                // Si el usuario existe
                if (numFila == 1) {
                    exit.writeUTF("Existe");
                } else {
                    
                    int r;
                    
                    consulta = "INSERT INTO cuenta(creditos, juegos, ganados, perdidos) VALUES(0, 0, 0, 0)";
                    query = conect.createStatement();
                    r = query.executeUpdate(consulta);
                    
                    consulta = "SELECT idCuenta FROM cuenta;";
                    query = conect.createStatement();
                    resultado = query.executeQuery(consulta);
                    int idCuenta = 0;
                    resultado.afterLast();
                    if(resultado.previous())
                        idCuenta = resultado.getInt(1);
                    consulta = "INSERT INTO usuarios(nombreUsuario, password, idCuenta) VALUES(\"" +line[1]+"\", \""+ line[2]+"\", " +idCuenta +")";
                    r = query.executeUpdate(consulta);
                    if(r >= 1) 
                        exit.writeUTF("OK");
                    
                    
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
    }
}