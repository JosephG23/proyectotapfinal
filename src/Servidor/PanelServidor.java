package Servidor;
public class PanelServidor extends javax.swing.JPanel {
    public PanelServidor() {
        initComponents();
    }

    void addEventos(OyenteServidor oyente){
        botonCerrar.addActionListener(oyente);
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        panelBotones = new javax.swing.JPanel();
        botonCerrar = new javax.swing.JButton();
        panelTabla = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaUsuarios = new javax.swing.JTable();
        panelTitulo = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(204, 255, 204));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setLayout(new java.awt.BorderLayout());

        panelBotones.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        botonCerrar.setText("Cerrar");
        botonCerrar.setName("cerrar");
        botonCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarActionPerformed(evt);
            }
        });
        panelBotones.add(botonCerrar);

        add(panelBotones, java.awt.BorderLayout.SOUTH);

        panelTabla.setPreferredSize(new java.awt.Dimension(400, 350));

        jScrollPane1.setPreferredSize(new java.awt.Dimension(500, 250));

        tablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Usuarios", "Juegos jugados", "Creditos", "Has ganado", "Has perdido"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tablaUsuarios.setAlignmentX(1.0F);
        jScrollPane1.setViewportView(tablaUsuarios);

        panelTabla.add(jScrollPane1);

        add(panelTabla, java.awt.BorderLayout.CENTER);

        jLabel1.setFont(new java.awt.Font("Ubuntu Light", 1, 18));
        jLabel1.setText("Servidor");
        panelTitulo.add(jLabel1);

        add(panelTitulo, java.awt.BorderLayout.NORTH);
    }

    private void botonCerrarActionPerformed(java.awt.event.ActionEvent evt) {

    }
    private javax.swing.JButton botonCerrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelTabla;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JTable tablaUsuarios;

    public javax.swing.JTable getTablaUsuarios() {
        return tablaUsuarios;
    }
}