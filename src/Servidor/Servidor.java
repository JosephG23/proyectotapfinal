package Servidor;
import javax.swing.JFrame;
public class Servidor {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Servidor del juego");
        PanelServidor juego = new PanelServidor();
        OyenteServidor oyente = new OyenteServidor(juego);
        juego.addEventos(oyente);
        frame.setSize(600, 400);
        frame.setLocation(100,100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(juego);
        frame.setVisible(true);
    }
    
}